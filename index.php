<?php
/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');


function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $str = '';
    $max = strlen($alphabet) - 1;
    for ($i=0; $i < 8; $i++)
        $str .=  $alphabet[rand(0, $max)];
        
        return $str;
}

$random_pass = randomPassword();

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Массив для временного хранения сообщений пользователю.
    $messages = array();
    
    if (!empty($_COOKIE['exist'])) {
        $messages['save'] = '<div>Login is taken</div>';
    }
    // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
    // Выдаем сообщение об успешном сохранении.
    if (!empty($_COOKIE['save'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('save', '', 100000);
        setcookie('exist', '', 100000);
        setcookie('login', '', 100000);
        setcookie('password', '', 100000);
        // Выводим сообщение пользователю.
        
        $messages[] = 'Thank you, information is saved <br/>';
        
        // Если в куках есть пароль, то выводим сообщение.
        if (!empty($_COOKIE['password'])) {
            $messages[] = sprintf('<div>You can <a href="login.php">get in</a> with a login <strong>%s</strong>
        and password <strong>%s</strong> to change the info.</div>',
                strip_tags($_COOKIE['login']),
                strip_tags($_COOKIE['password']));
        }
    }
    
    // Складываем признак ошибок в массив.
    $errors = array();
    
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    
    $errors['mail'] = !empty($_COOKIE['mail_error']);
    
    $errors['data'] = !empty($_COOKIE['data_error']);
    
    $errors['sex'] = !empty($_COOKIE['sex_error']);
    
    $errors['limb'] = !empty($_COOKIE['limb_error']);
    
    $errors['suppow'] = !empty($_COOKIE['suppow_error']);
    
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    
    $errors['check'] = !empty($_COOKIE['check_error']);
    
    // Выдаем сообщения об ошибках.
    if (!empty($errors['fio'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('fio_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">Input your full name.</div>';
    }
    if ($errors['mail']) {
        setcookie('mail_error', '', 100000);
        $messages[] = '<div class="error">Input your e-mail.</div>';
    }
    
    if ($errors['data']) {
        setcookie('data_error', '', 100000);
        $messages[] = '<div class="error">Input your birth date.</div>';
    }
    
    if ($errors['sex']) {
        setcookie('sex_error', '', 100000);
        $messages[] = '<div class="error">Choose your gender.</div>';
    }
    
    if ($errors['limb']) {
        setcookie('limb_error', '', 100000);
        $messages[] = '<div class="error">Choose the number of limbs you have.</div>';
    }
    
    if ($errors['suppow']) {
        setcookie('suppow_error', '', 100000);
        $messages[] = '<div class="error">Choose the super powers you have.</div>';
    }
    
    if ($errors['bio']) {
        setcookie('bio_error', '', 100000);
        $messages[] = '<div class="error">Write smth about yourself.</div>';
    }
    
    if ($errors['check']) {
        setcookie('check_error', '', 100000);
        $messages[] = '<div class="error">Check if you agree.</div>';
    }
    
    // Складываем предыдущие значения полей в массив, если есть.
    // При этом санитизуем все данные для безопасного отображения в браузере.
    $values = array();
    
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
    
    $values['mail'] = empty($_COOKIE['mail_value']) ? '' : strip_tags($_COOKIE['mail_value']);
    
    $values['data'] = empty($_COOKIE['data_value']) ? '' : strip_tags($_COOKIE['data_value']);
    
    $values['sex'] = empty($_COOKIE['sex_value']) ? '' : strip_tags($_COOKIE['sex_value']);
    
    $values['limb'] = empty($_COOKIE['limb_value']) ? '' : strip_tags($_COOKIE['limb_value']);
    
    $values['suppow'] = empty($_COOKIE['suppow_value']) ? '' : strip_tags($_COOKIE['suppow_value']);
    
    $values['bio'] = empty($_COOKIE['bio_value']) ? '' : strip_tags($_COOKIE['bio_value']);
    
    $values['check'] = empty($_COOKIE['check_value']) ? '' : strip_tags($_COOKIE['check_value']);
    
    // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
    // ранее в сессию записан факт успешного логина.
    if (!empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {
            // TODO: загрузить данные пользователя из БД
            // и заполнить переменную $values,
            // предварительно санитизовав.
            
            $user = 'u20389';
            $pass = '4147831';
            $db = new PDO('mysql:host=localhost;dbname=u20389', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
            
            // Подготовленный запрос. Не именованные метки.
            try {
                
                $stmt = $db->prepare("SELECT fio FROM application1 WHERE login = ?");
                $stmt -> execute([$_SESSION['login']]);
                $values['fio'] = $stmt->fetchColumn();
                
                $stmt = $db->prepare("SELECT mail FROM application1 WHERE login = ?");
                $stmt -> execute([$_SESSION['login']]);
                $values['mail'] = $stmt->fetchColumn();
                
                $stmt = $db->prepare("SELECT data FROM application1 WHERE login = ?");
                $stmt->execute([$_SESSION['login']]);
                $values['data'] = $stmt->fetchColumn();
                
                $stmt = $db->prepare("SELECT sex FROM application1 WHERE login = ?");
                $stmt->execute([$_SESSION['login']]);
                $values['sex'] = $stmt->fetchColumn();
                
                $stmt = $db->prepare("SELECT limbs FROM application1 WHERE login = ?");
                $stmt->execute([$_SESSION['login']]);
                $values['limb'] = $stmt->fetchColumn();
                
                $stmt = $db->prepare("SELECT abilities FROM application1 WHERE login = ?");
                $stmt->execute([$_SESSION['login']]);
                $values['suppow'] = $stmt->fetchColumn();
                
                $stmt = $db->prepare("SELECT bio FROM application1 WHERE login = ?");
                $stmt->execute([$_SESSION['login']]);
                $values['bio'] = $stmt->fetchColumn();
                
                
            }
            catch(PDOException $e){
                print('Error : ' . $e->getMessage());
                exit();
            }
            
            $messages[] ='Using login ';
            $messages[] = $_SESSION['login'];
        }
        
        // Включаем содержимое файла form.php.
        // В нем будут доступны переменные $messages, $errors и $values для вывода
        // сообщений, полей с ранее заполненными данными и признаками ошибок.
        include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
    // Проверяем ошибки.
    $errors = FALSE;
    
    if (empty($_POST['fio']) || strlen($_POST['fio']) < 2 || preg_match('/^[A-Za-zА-Яа-я0-9\s]+$/u',$_POST['fio'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['mail']) || filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL) === FALSE ) {
        setcookie('mail_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('mail_value', $_POST['mail'], time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['data'])) {
        setcookie('data_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('data_value', $_POST['data'], time() + 30 * 24 * 60 * 60);
    }
    
    if ( empty($_POST['sex'])) {
        setcookie('sex_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        switch($_POST['sex']) {
            case 'm': {
                $sex='m';
                break;
            }
            case 'f':{
                $sex='f';
                break;
            }
        };
        
        setcookie('sex_value', $sex, time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['Limbs'])) {
        setcookie('limb_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        switch($_POST['Limbs']) {
            case '1': {
                $limbs='1';
                break;
            }
            case '2':{
                $limbs='2';
                break;
            }
            case '3':{
                $limbs='3';
                break;
            }
            case '4':{
                $limbs='4';
                break;
            }
        };
        
        setcookie('limb_value', $limbs, time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['Super_powers'])) {
        setcookie('suppow_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        
        $powers=serialize($_POST['Super_powers']);
        
        setcookie('suppow_value', $powers, time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['bio'])) {
        setcookie('bio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('bio_value', $_POST['bio'], time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['agree'])) {
        setcookie('check_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        $agree = $_POST['agree'] ? '1' : '0';
        setcookie('check_value', $agree, time() + 30 * 24 * 60 * 60);
    }
    
    
    if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        header('Location: index.php');
        exit();
    }
    else {
        // Удаляем Cookies с признаками ошибок.
        setcookie('fio_error', '', 100000);
        setcookie('mail_error', '', 100000);
        setcookie('data_error', '', 100000);
        setcookie('sex_error', '', 100000);
        setcookie('limb_error', '', 100000);
        setcookie('suppow_error', '', 100000);
        setcookie('bio_error', '', 100000);
        setcookie('check_error', '', 100000);
    }

    $safefio=htmlspecialchars($_POST['fio']);
    
    $safebio=htmlspecialchars($_POST['bio']);

    switch($_POST['sex']) {
        case 'm': {
            $sex='m';
            break;
        }
        case 'f':{
            $sex='f';
            break;
        }
    };
    
    switch($_POST['Limbs']) {
        case '1': {
            $limbs='1';
            break;
        }
        case '2':{
            $limbs='2';
            break;
        }
        case '3':{
            $limbs='3';
            break;
        }
        case '4':{
            $limbs='4';
            break;
        }
    };
    
    $abilities = serialize($_POST['Super_powers']);
    
    if (!empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {
            // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
            $user = 'u20389';
            $pass = '4147831';
            $db = new PDO('mysql:host=localhost;dbname=u20389', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
            
            // Подготовленный запрос. Не именованные метки.
            try {
                $stmt = $db->prepare("UPDATE application1 SET mail = ?, data = ? ,sex = ?, limbs = ?, abilities = ?, bio = ? WHERE login = ?");
                $stmt -> execute(array($_POST['mail'],$_POST['data'],$sex,$limbs,$abilities,$safebio,$_SESSION['login']));
            }
            catch(PDOException $e){
                print('Error : ' . $e->getMessage());
                exit();
            }
            setcookie('save', '1');
        }
        else {
            // Генерируем уникальный логин и пароль.
            // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
            $user = 'u20389';
            $pass = '4147831';
            $db = new PDO('mysql:host=localhost;dbname=u20389', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
            // Генерируем уникальный логин и пароль.
            // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
            try {
                $stmt = $db->prepare("SELECT login FROM application1 WHERE login = ?");
                $stmt -> execute([$_POST['login']]);
                $log = $stmt->fetchColumn();
            }
            catch(PDOException $e){
                print('Error : ' . $e->getMessage());
                exit();
            }
            if ($log != NULL){
                setcookie('exist', '1');
            }
            else {
                $a = substr($_POST['fio'], 0, strpos($_POST['fio'], ' ' ));
                for ($i= 0; $i < 2; $i++)
                $a .= rand(0,9);
                
                
                $login = $a;
                $password = $random_pass;
                // Сохраняем в Cookies.
                setcookie('login', $login);
                setcookie('password', $password);
                
                // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
                $password1 = md5($password);

                // Подготовленный запрос. Не именованные метки.
                try {
                    $stmt = $db->prepare("INSERT INTO application1 SET fio = ?, mail = ?, data = ?, sex = ?, limbs = ?, abilities = ?, bio = ?, login = ?, password = ?");
                    $stmt -> execute(array($safefio,$_POST['mail'],$_POST['data'],$sex,$limbs,$abilities,$safebio,$login,$password1));
                }
                catch(PDOException $e){
                    print('Error : ' . $e->getMessage());
                    exit();
                }
                // Сохраняем куку с признаком успешного сохранения.
                setcookie('save', '1');
            }
        }
        
        
        $new_url = 'index.php';
        header('Location: '.$new_url);
        
}
?>