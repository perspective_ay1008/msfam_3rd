<html>
  <head>
  <link rel='stylesheet' href='style.css'>
    <title>Main</title>
     <style>
     #messages{
     margin:o auto;
     text-align:center;
     margin:5px;
     font-size:20px;
     display:flex;
     flex-direction:column;
     align-content: center;
     padding-bottom:15px;
     }
     .error {
     border: 1px solid red;
     color: red!important;
     height:25px;
     background-color: #f6eceb;
     margin:3px;
    }
     .errord {
     border: 2px solid red;
    }
    .btns{
    margin:o auto;
     display: flex;
     flex-direction: row;
     justify-content: space-evenly;
     max-width: 100%;
     width: 700px;
    }
    .sbtns{
     display: flex;
     flex-direction: column;
     align-content:center;
    }
    button{
    background-color: #f6eceb;
    font-size:20px;
    color:black;
    height:35px;
    border-radius: 10px;
    border: 2px solid #f6eceb;
    }
    .exit{
    color: red;
    border:1px solid #F08080;
    margin-left:auto;   
    }
    .log{
    margin-bottom:7px;
    }
    button:hover{
    box-shadow:0 0 10px #017272;
    background: #142d36;
    border: 2px solid #142d36;
    color:#f6eceb;
    }
    .exit:hover{
    box-shadow: 0 0 10px #8B0000;
    background: #fabebe;
    border: 2px solid #F08080;
    color:#f6eceb;
    }
    </style>
  </head>
<body>
   <main>
   <div class="btns">
  <div class="sbtns">
  <button name="submit" class="log" onclick="document.location.replace('login.php');">Login</button>
  <button name="submit" onclick="document.location.replace('admin.php');">Login as admin</button>
  </div>
  <button class="exit" name="submit" onclick="document.location.replace('logout.php');">Logout</button><br>
  </div>
            <h3>Form</h3> <br>
   
<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}
// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>

        <form action="" method="POST">
            <fieldset class="blue">
                <fieldset class="top">
                <p>Input your full name</p>
                <label>
                 <input name="fio" type="text" placeholder="fio" class="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>"/>
            </label>
            <br>
                <p>Input your e-mail</p>
                 <label> 
                <input name="mail" type="email" placeholder="e-mail" <?php if ($errors['mail']) {print 'class="errord"';} ?> value="<?php print $values['mail']; ?>"/>
            </label><br>
                <p>Input your birth date</p> 
                <label title="DOB"> 
                <input type="date" name="data" <?php if ($errors['data']) {print 'class="errord"';} ?> value="<?php print $values['data']; ?>"/>
            </label></fieldset><br>
        <fieldset> 
                <p>Sex</p> 
                <label>
                     <input type="radio" value="m" name="sex" class="rad" <?php if ('m' === $values['sex']){print 'checked="checked"';}  ?>/> Male
                </label>
                <label>
                     <input type="radio" value="f" name="sex" class="rad" <?php if ('f' === $values['sex']){print 'checked="checked"';}  ?>/> Female
                </label>
            <br>
                <p>Number of limbs</p>
                <label>
                    <input type="radio" value="1" name="Limbs" class="rad" <?php if ('1' === $values['limb']){print 'checked="checked"';}  ?>/>1
                </label>
                <label>
                    <input type="radio" value="2" name="Limbs" class="rad" <?php if ('2' === $values['limb']){print 'checked="checked"';}  ?>/>2
                </label>
                <label>
                    <input type="radio" value="3" name="Limbs" class="rad" <?php if ('3' === $values['limb']){print 'checked="checked"';}  ?>/>3
                </label>
                <label>
                    <input type="radio" value="4" name="Limbs" class="rad" <?php if ('4' === $values['limb']){print 'checked="checked"';}  ?>/>4
                </label>
            <br>
                <p>Superpowers</p>
                <label>
                <select name="Super_powers[]" multiple size="3" class="bord">
                     <option value="immortal" <?php if (strpos($values['suppow'],'immortal')){print 'selected';} ?> >Immortality</option>
                   <option value="go_through_walls" <?php if (strpos($values['suppow'],'go_through_walls')){print 'selected';} ?> >Going through walls</option>
                     <option value="fly" <?php if (strpos($values['suppow'],'fly')){print 'selected';} ?> >Flying</option>
                </select>
            </label></fieldset>
            <br>
        <fieldset> <p>Biography</p>
            <textarea  id="bio" name="bio" class="bord" <?php if ($errors['bio']) {print 'class="error"';} ?>><?php print $values['bio']; ?></textarea></fieldset><br>
        <fieldset>
                <input type="checkbox" name="agree" class="check" <?php if ($errors['check']) {print 'class="error"';} ?> <?php if ('1' === $values['check']){print 'checked="checked"';}  ?>/>
                I give permission to use my personal data
            </fieldset><br>
            <input class="submit" type="submit" value="Submit"/>
        </fieldset>
        </form>
</main>
</body>
</html>