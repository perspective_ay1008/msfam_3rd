
<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
  // Если есть логин в сессии, то пользователь уже авторизован.
  // TODO: Сделать выход (окончание сессии вызовом session_destroy()
  //при нажатии на кнопку Выход).
  // Делаем перенаправление на форму.
  header('Location: ./');
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
?>
<html>
  <head>
  <link rel='stylesheet' href='style.css'>
    <title>Login Page</title>
    <style>
    body{
    background: linear-gradient(45deg, #EECFBA, #C5DDE8);
    }
    form{
    margin-top:auto;
    margin-bottom:auto;
    display:flex;
    flex-direction:column;
    }
    .submit{
    margin:0 auto;
    margin-top:10px;
     border:1px solid #F08080;
    box-shadow: 0 0 10px #8B0000;
    }
    .submit:hover{
    background: #fabebe;
    border: 2px solid #F08080;
    color:#f6eceb;
    }
    .pink {
    border:1px solid #F08080;
    box-shadow: 0 0 10px #8B0000;
    background-color:fabebe;
    margin-bottom: 10px;
    }
    input{
     max-width:300px;
     width:300px;
     margin:10px;
     border:1px solid #F08080;
    }
    </style>
    </head>
    <body> 
<form action="" method="post">
<fieldset class="pink">
  <p>Input your login</p>
  <input name="login" />
  <p>Input your password</p>
  <input name="password" />
 </fieldset>
  <input class="submit" type="submit" value="access" />
</form>
</body>
</html>
<?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {

    $user = 'u20389';
    $pass = '4147831';
    $db = new PDO('mysql:host=localhost;dbname=u20389', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    
    // Подготовленный запрос. Не именованные метки.
    try {
        $stmt = $db->prepare("SELECT password FROM application1 WHERE login = ?");
        $stmt -> execute(array($_POST['login']));
        $pass1 = $stmt->fetch();
        
        if (md5($_POST['password']) == $pass1[0]){
            // Если все ок, то авторизуем пользователя.
            $_SESSION['login'] = $_POST['login'];
            
            // Делаем перенаправление.
            header('Location: ./');
        }
        else {
            header('Location: ./login.php');
        }
    }
    catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
    }
}
?>